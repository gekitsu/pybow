#!/usr/bin/env python3
from context import pybow as pb
import pytest

import pandas as pd


# test each function that does more than simple math on its inputs

def test_get_bowfactors():
    assert pb.analysis.junk13._get_bowfactors(145) == (65.0, 0.88)
    assert pb.analysis.junk13._get_bowfactors(50) == (20.0, 0.09)
    assert pb.analysis.junk13._get_bowfactors(180) == (70.0, 1.00)


# approximation test for junkmanns2013 with rotten bottom artefact data
@pytest.fixture
def junkmanns_rb():
    return pd.DataFrame([[0, 0.9, 0.75],
                         [8, 1.58, 0.84],
                         [18, 2.0, 1.16],
                         [28, 2.15, 1.21],
                         [38, 2.71, 1.49],
                         [48, 2.69, 1.5],
                         [58, 2.64, 1.52],
                         [63, 2.7, 1.65],
                         [68, 2.55, 1.97],
                         [78, 2.11, 2.45],
                         [88, 1.9, 3.3]],
                        columns=['l', 'width', 'thickness'])


def test_junkmanns2013(junkmanns_rb):
    jj_cf = 5.6
    jj_spread = 0.35
    jj_results, _ = pb.analysis.junkmanns2013(junkmanns_rb, jj_cf, jj_spread,
                                              name='Rotten Bottom')
    assert jj_results['drawweight'] * 0.2248 == pytest.approx(25, abs=3)
