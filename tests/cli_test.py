#!/usr/bin/env python3
from context import pybow as pb
import pytest


@pytest.fixture
def foofunc():
    def foo(arg1, arg2, kwarg1='defaultstr'):
        """foo

        Args:
            arg1: argument 1
            arg2: argument 2

        Kwargs:
            kwarg1 (str): kwarg one

        Returns:
            Something.
        """
        return True
    return foo


@pytest.fixture
def barfunc():
    def bar(arg1, kwarg1=True, kwarg2=None):
        """bar

        Args:
            arg1: argument 1

        Kwargs:
            kwarg1 (bool): kwarg one
            kwarg2 (int): kwarg two

        Returns:
            Something.
        """
        return False
    return bar


def test_safe_type_eval():
    assert pb.cli._safe_type_eval('str') == str
    assert pb.cli._safe_type_eval('foo') is None


def test_make_dsdict():
    ds = """Short function description.

    Long function description that spans multiple lines.
    This is the second line.

    Args:
        arg1 (str): An arg with str type and a 2-line description.
          This is its second line.
        arg2: An arg without a type.

    Kwargs:
        kwarg1 (bool): A kwarg with type bool.
        kwarg2 (foo): A kwarg with a non-legit type.

    Returns:
        Nothing of value."""
    ds_dict = pb.cli._make_docstringdict(ds)
    assert len(ds_dict) == 4
    assert ds_dict['arg1'].help == ("An arg with str type and a 2-line "
                                    "description. This is its second line.")
    assert ds_dict['arg1'].type == str
    assert ds_dict['arg2'].type is None
    assert ds_dict['kwarg2'].type is None


def test_configure_parser(foofunc, barfunc, tmpdir):
    parser = pb.cli._configure_parser([foofunc, barfunc])
    parse1 = parser.parse_args('foo a1 a2 --kwarg1 foofoo'.split())
    assert parse1.func == foofunc
    assert parse1.arg1 == 'a1'
    assert parse1.kwarg1 == 'foofoo'

    outfile = tmpdir.join('file.json')
    p2args = 'bar a1 --no-kwarg1 --output '+str(outfile)
    parse2 = parser.parse_args(p2args.split())
    assert parse2.func == barfunc
    assert parse2.arg1 == 'a1'
    assert parse2.kwarg1 is False
    assert parse2.output.mode == 'w'

    with pytest.raises(SystemExit):
        parser.parse_args('bar a1 --kwarg2 asdf'.split())
