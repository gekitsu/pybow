Beam Theory Properties
----------------------

.. module:: pybow

The naming conventions for these functions are as follows:

Second moment of area:
   ``I``

Section modulus:
   ``W`` (after EUROCODE 3, because North American and British/Australian notation for elastic and plastic section modulus both use ``S`` and ``Z``, but are inverse of each other.

Maximum neutral plane distance:
   ``e_back`` for distance to the bow’s back, the tension side.
   ``e_belly`` for distance to the bow’s belly, the compression side.
   ``e`` for the larger of the two.

Rectangle
:::::::::

Formulas for a rectangular cross-section:

.. function:: rect_I(w, t)

   Return second moment of area for a rectangular cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`I`
   :rtype: float

.. function:: rect_W(w, t)

   Return section modulus for a rectangular cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`W`
   :rtype: float

.. function:: rect_e_back(w, t)

   Return distance neutral plane to back for a rectangular cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`e_{back}`
   :rtype: float

.. function:: rect_e_belly(w, t)

   Return distance neutral plane to belly for a rectangular cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`e_{belly}`
   :rtype: float

.. function:: rect_e(w, t)

   Return maximum distance from the neutral plane for a rectangular cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`e`
   :rtype: float

Ellipse
:::::::

Formulas for a rounded cross-section of variable width and thickness, i.e. an ellipse:

.. function:: round_I(w, t)

   Return second moment of area for an elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`I`
   :rtype: float

.. function:: round_W(w, t)

   Return section modulus for an elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`W`
   :rtype: float

.. function:: round_e_back(w, t)

   Return distance neutral plane to back for an elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`e_{back}`
   :rtype: float

.. function:: round_e_belly(w, t)

   Return distance neutral plane to belly for an elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`e_{belly}`
   :rtype: float

.. function:: round_e(w, t)

   Return maximum distance from the neutral plane for an elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`e`
   :rtype: float

Half-Ellipse (Round Back)
:::::::::::::::::::::::::

Formulas for a semi-elliptic cross-section with the flat side being the belly and the rounded side being the back.

.. function:: halfround_I(w, t)

   Return second moment of area for a semi-elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`I`
   :rtype: float

.. function:: halfround_W(w, t)

   Return section modulus for a semi-elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`W`
   :rtype: float

.. function:: halfround_e_back(w, t)

   Return distance neutral plane to back for a semi-elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`e_{back}`
   :rtype: float

.. function:: halfround_e_belly(w, t)

   Return distance neutral plane to belly for a semi-elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`e_{belly}`
   :rtype: float

.. function:: halfround_e(w, t)

   Return maximum distance from the neutral plane for a semi-elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`e`
   :rtype: float

Half-Ellipse (Flat Back)
::::::::::::::::::::::::

Formulas for a semi-elliptic cross-section with the flat side being the back and the rounded side being the belly.
The name ‘english’ is meant t be an easily recallable mnemonic, deriving from the stereotypical english longbow cross-section being rounded on the belly side.

.. function:: english_I(w, t)

   Return second moment of area for an inverted semi-elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`I`
   :rtype: float

.. function:: english_W(w, t)

   Return section modulus for an inverted semi-elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`W`
   :rtype: float

.. function:: english_e_back(w, t)

   Return distance neutral plane to back for an inverted semi-elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`e_{back}`
   :rtype: float

.. function:: english_e_belly(w, t)

   Return distance neutral plane to belly for an inverted semi-elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`e_{belly}`
   :rtype: float

.. function:: english_e(w, t)

   Return maximum distance from the neutral plane for an inverted semi-elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :returns: :math:`e`
   :rtype: float

Trapezium
:::::::::

Formulas for a trapezoid cross-section with parallel belly and back sides, but with them being of unequal lengths, resulting in a tapered profile.
Trapeziums require an additional input argument describing the ratio of back to belly side lengths.
A value of 1.0 describes a rectangle, values smaller than 1.0 describe narrowed back sides, values larger than 1.0 describe narrowed belly sides.

.. function:: trap_I(w, t, ratio)

   Return second moment of area for a traezoidal cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :param float ratio: Ratio of back to belly side width.
   :returns: :math:`I`
   :rtype: float

.. function:: trap_W(w, t, ratio)

   Return section modulus for a traezoidal cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :param float ratio: Ratio of back to belly side width.
   :returns: :math:`W`
   :rtype: float

.. function:: trap_e_back(w, t, ratio)

   Return distance neutral plane to back for a traezoidal cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :param float ratio: Ratio of back to belly side width.
   :returns: :math:`e_{back}`
   :rtype: float

.. function:: trap_e_belly(w, t, ratio)

   Return distance neutral plane to belly for a traezoidal cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :param float ratio: Ratio of back to belly side width.
   :returns: :math:`e_{belly}`
   :rtype: float

.. function:: trap_e(w, t, ratio)

   Return maximum distance from the neutral plane for a traezoidal cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :param float ratio: Ratio of back to belly side width.
   :returns: :math:`e`
   :rtype: float

Bundle Functions
::::::::::::::::

In addition to the functions for single properties, there exists a suite of bundle functions that can provide arbitrary sets of properties with a single convenience function call.

.. function:: rect(w, t, props=['I', 'W', 'e'])

   Returns the properties listed in ``props`` for a rectangular cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :param list[str] props: A list of strings denoting the properties to be calculated.
                           Possible string values are the values outlined at the beginning of this chapter.
                           The default values are :math:`I`, :math:`W`, and :math:`e`.
   :returns: A list of the desired properties.
   :rtype: list[float]

.. function:: round(w, t, props=['I', 'W', 'e'])

   Returns the properties listed in ``props`` for an elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :param list[str] props: A list of strings denoting the properties to be calculated.
                           Possible string values are the values outlined at the beginning of this chapter.
                           The default values are :math:`I`, :math:`W`, and :math:`e`.
   :returns: A list of the desired properties.
   :rtype: list[float]

.. function:: halfround(w, t, props=['I', 'W', 'e'])

   Returns the properties listed in ``props`` for a semi-elliptic cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :param list[str] props: A list of strings denoting the properties to be calculated.
                           Possible string values are the values outlined at the beginning of this chapter.
                           The default values are :math:`I`, :math:`W`, and :math:`e`.
   :returns: A list of the desired properties.
   :rtype: list[float]

.. function:: english(w, t, props=['I', 'W', 'e'])

   Returns the properties listed in ``props`` for an inverted semi-elliptical cross-section.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :param list[str] props: A list of strings denoting the properties to be calculated.
                           Possible string values are the values outlined at the beginning of this chapter.
                           The default values are :math:`I`, :math:`W`, and :math:`e`.
   :returns: A list of the desired properties.
   :rtype: list[float]

.. function:: trap50(w, t, props=['I', 'W', 'e'])

   Returns the properties listed in ``props`` for a trapezoidal cross-section with ratio :math:`frac{1}{2}`.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :param list[str] props: A list of strings denoting the properties to be calculated.
                           Possible string values are the values outlined at the beginning of this chapter.
                           The default values are :math:`I`, :math:`W`, and :math:`e`.
   :returns: A list of the desired properties.
   :rtype: list[float]

.. function:: trap66(w, t, props=['I', 'W', 'e'])

   Returns the properties listed in ``props`` for a trapezoidal cross-section with ratio :math:`frac{2}{3}`

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :param list[str] props: A list of strings denoting the properties to be calculated.
                           Possible string values are the values outlined at the beginning of this chapter.
                           The default values are :math:`I`, :math:`W`, and :math:`e`.
   :returns: A list of the desired properties.
   :rtype: list[float]

.. function:: trap75(w, t, props=['I', 'W', 'e'])

   Returns the properties listed in ``props`` for a trapezoidal cross-section with ratio :math:`frac{3}{4}`.

   :param float w: Cross-section width.
   :param float t: Cross-section thickness.
   :param list[str] props: A list of strings denoting the properties to be calculated.
                           Possible string values are the values outlined at the beginning of this chapter.
                           The default values are :math:`I`, :math:`W`, and :math:`e`.
   :returns: A list of the desired properties.
   :rtype: list[float]
