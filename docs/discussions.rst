Topical Discussions
===================

Method Backgrounds
------------------

.. toctree::
   :glob:
   
   discussions/methods_*

Design Decisions
----------------

.. toctree::
   :glob:
   
   discussions/design_*
