#!/usr/bin/env python3
from context import pybow as pb
import pytest

from io import StringIO
import numpy as np
import pandas as pd

# CONSTANTS & FIXTURES

VALID_LIMB_DF = pd.DataFrame([[-3.0, 2.0, 2.0],
                              [0.0, 1.0, 1.0],
                              [5.0, 3.0, 1.0],
                              [10.0, 5.0, 1.3],
                              [15.0, 7.0, 1.5]],
                             columns=['l', 'width', 'thickness'])
INVALID_LIMB_DF = pd.DataFrame([[1, 2, 3]], columns=['l', 'width', 'foobar'])
INVALID_LIMB_DF2 = pd.DataFrame([[1, 2, 'foo']],
                                columns=['l', 'width', 'thickness'])

RB_STR = """name: Rotten Bottom
material: Yew
source: Junkmanns 2013, p.199
limbs:
- data: rb_0.csv
- data: rb_1.csv
  complete: false
"""

RB1_STR = """l,width,thickness
0,0.9,0.75
8,1.58,0.84
18,2.0,1.16
28,2.15,1.21
38,2.71,1.49
48,2.69,1.5
58,2.64,1.52
63,2.7,1.65
68,2.55,1.97
78,2.11,2.45
88,1.9,3.3
"""

RB2_STR = """l,width,thickness
0,2.77,1.51
5,2.72,1.65
15,2.27,2.32
25,1.9,3.3
"""


@pytest.fixture
def blank_bow():
    return pb.Bow('testname', 'testsource')


@pytest.fixture
def sample_bow():
    bow = blank_bow()
    bow.add_limb(VALID_LIMB_DF, tags=['position:top'])
    bow.add_limb(VALID_LIMB_DF)
    return bow


@pytest.fixture
def rb_bow():
    bow = pb.Bow('Rotten Bottom', 'Junkmanns 2013, p.199', material='Yew')
    bow.add_limb(pd.read_csv(StringIO(RB1_STR), index_col=None))
    bow.add_limb(pd.read_csv(StringIO(RB2_STR), index_col=None),
                 complete=False)
    return bow


def test_bow_init():
    a = pb.Bow('testname', 'testsource')
    assert a.name == 'testname', a.source == 'testsource'


def test_bow_init_with_limbs_kwarg():
    a = pb.Bow('testname', 'testsource',
               limbs=[[VALID_LIMB_DF, False, ['position:top']], VALID_LIMB_DF])
    assert len(a) == 2
    assert 'position:top' in a[0].tags
    assert a[0].complete is False
    assert a[1].complete is True


def test_bow_len(blank_bow):
    blank_bow._limbs = ('a')
    assert len(blank_bow) == 1


def test_bow_indexing(blank_bow):
    blank_bow._limbs = ('a')
    assert blank_bow[0] == 'a'


def test_bow_indexing_by_tag(sample_bow):
    assert len([l for l in sample_bow if 'position:top' in l.tags]) == 1


def test_bow_indexing_indexerror(blank_bow):
    blank_bow._limbs = ('a')
    with pytest.raises(IndexError):
        blank_bow[1]


def test_bow_str(rb_bow):
    rb_bow.tags |= {'type:bodman'}
    rb_bow.description = 'A description that is more than 32 characters long.'
    expected_str = 'Rotten Bottom\n' \
                   '(Junkmanns 2013, p.199)\n' \
                   '--------------------------------\n' \
                   'date: None\n' \
                   'material: Yew\n' \
                   'tags: type:bodman\n' \
                   'description: A description that\n' \
                   '  is more than 32 characters\n' \
                   '  long.'
    assert str(rb_bow) == expected_str


def test_bow_repr(sample_bow):
    eval_bow = eval(repr(sample_bow))
    assert isinstance(eval_bow, pb.Bow)
    assert eval_bow.name == sample_bow.name
    assert len(eval_bow) == len(sample_bow)


def test_limb_adding(blank_bow):
    blank_bow.add_limb(VALID_LIMB_DF)
    assert isinstance(blank_bow[0], pb.Limb)


def test_max_limbs(sample_bow):
    with pytest.raises(TypeError):
        sample_bow.add_limb(VALID_LIMB_DF)
    assert len(sample_bow) == 2


def test_limb_deleting(sample_bow):
    sample_bow.del_limb(0)
    assert len(sample_bow) == 1


def test_limb_deleting_indexerror(sample_bow):
    sample_bow.del_limb(1)
    with pytest.raises(IndexError):
        sample_bow.del_limb(1)


def test_limb_df_validation(blank_bow):
    with pytest.raises(ValueError):
        blank_bow.add_limb(INVALID_LIMB_DF)
    with pytest.raises(TypeError):
        blank_bow.add_limb(INVALID_LIMB_DF2)
    assert len(blank_bow) == 0


def test_limb_prop_material(blank_bow):
    blank_bow.material = 'testmaterial'
    blank_bow.add_limb(VALID_LIMB_DF)
    assert blank_bow[0].material == blank_bow.material


def test_limb_prop_limbnames(sample_bow):
    assert sample_bow[0].name == sample_bow.name+'_0'
    assert sample_bow[1].name == sample_bow.name+'_1'


def test_limb_prop_length(sample_bow):
    assert sample_bow[0].length == 15.0


def test_limb_prop_abslength(sample_bow):
    assert sample_bow[0].abslength == 18.0


def test_bow_prop_abslength(sample_bow):
    assert sample_bow.abslength == 36.0


def test_bow_prop_ntnlength_value(sample_bow):
    assert sample_bow.ntnlength == 30.0


def test_bow_prop_ntnlength_nan(sample_bow):
    sample_bow[1].complete = False
    assert np.isnan(sample_bow.ntnlength)


def test_bow_read(tmpdir, rb_bow):
    yamlfile = tmpdir.join('rb.yaml')
    yamlfile.write(RB_STR)
    csvfile1 = tmpdir.join('rb_0.csv')
    csvfile1.write(RB1_STR)
    csvfile2 = tmpdir.join('rb_1.csv')
    csvfile2.write(RB2_STR)

    read_bow = pb.read(tmpdir.join('rb.yaml'))

    assert read_bow.name == rb_bow.name
    assert read_bow.material == rb_bow.material
    assert read_bow.source == rb_bow.source
    assert read_bow.abslength == rb_bow.abslength
    assert len(read_bow) == len(rb_bow)


def test_bow_read_error(tmpdir):
    yamlfile = tmpdir.join('bow.yaml')
    yamlfile.write("name: foo")  # no source!
    with pytest.raises(ValueError):
        pb.read(yamlfile)


def test_bow_write(tmpdir, rb_bow):
    rb_bow.write(tmpdir.join('rb.yaml'))

    assert tmpdir.join('rb.yaml').read() == RB_STR
    assert tmpdir.join('rb_0.csv').read() == RB1_STR
    assert tmpdir.join('rb_1.csv').read() == RB2_STR
