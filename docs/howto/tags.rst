Using the Tag System
--------------------

The tag system allows you to store any number of user-defined category descriptors for either Bows or their limbs, like bow type, for example.
These descriptors are stored in the ``tags`` attribute.
Both ``Bow`` and ``Limb`` objects have a ``tags`` attribute.
The attribute holds a set::

   >>> import pybow as pb
   >>> vrees = pb.data.beckhoff1964_vrees
   >>> vrees.tags
   set()


Adding Tags
:::::::::::

To assign tags to a bow, use either::

   >>> vrees.tags.add('type:vrees')

to add a single tag, or::

   >>> vrees.tags |= {'type:vrees', 'country:ger'}

for adding a set of tags at once.

Filtering by Tag
::::::::::::::::

Given a corpus of bows, you filter by tag similar to how you filter by something like ``limb.complete``::

   >>> rb = pb.data.junkmanns2013_rottenbottom
   >>> rb.tags
   {'type:bodman'}
   >>> vrees.tags
   {'type:vrees', 'country:ger'}
   >>> corpus = [rb, vrees]
   >>> bodman_bows = []
   >>> for bow in corpus:
   ...     if 'type:bodman' in bow.tags:
   ...         bodman_bows.append(bow)
   >>> len(bodman_bows)
   1

Or, the more concise version::

   >>> bodman_bows_2 = [bow
   ...     for bow in corpus
   ...     if 'type:bodman' in bow.tags]
   >>> len(bodman_bows_2)
   1

And the proof that these do the same thing::

   >>> bodman_bows == bodman_bows_2
   True

.. note::
   For set operation and comparison syntax, it is worth checking the relevant chapter of the `Python Documentation <https://docs.python.org/3/library/stdtypes.html#set>`_.
