Tutorials
=========

Run a Simple Artefact Analysis
------------------------------

In this tutorial, we are going to run a simple analysis of a bow.
This means we have a bow’s measurements and want to know details about that bow’s usage – its draw weight, draw length, and so on.
For this, we will be using Beckhoff’s analysis method from a 1964 paper he wrote for a german archaeology magazine.

Required prerequisites:

* Python with the pybow package installed.
* Reasonably basic proficiency in Python.
* Very basic proficiency in Pandas: Making DataFrames. (`Pandas documentation: Intro to data structures <https://pandas.pydata.org/pandas-docs/stable/dsintro.html#dataframe>`_)

Step 0: Import all you need
:::::::::::::::::::::::::::

The first step is to import all the packages we need.
This will be pybow, of course, and the Pandas package for its DataFrame structures::

   >>> import pybow as pb
   >>> import pandas as pd

Step 1: Provide Bow Measurements
::::::::::::::::::::::::::::::::

Pybow operates on measurements stored in pandas.DataFrames.
Since most methods require the measurements of only half a bow, we will use a sequence of measurements from a bow’s nock to its midpoint.
All measurements are metric, in cm.

As a starting data set, we will use Junkmanns’ measurements of the Vrees bow artefact – the same bow Beckhoff predicted a draw weight of ~60 lbf. at a draw length just shy of 28″ for.
Measurements will be sequences of:

* bow width at a point,
* bow thickness at that point,
* and that point’s distance from the nock.

These columns will be called ``width``, ``thickness``, and ``l``, respectively::

   >>> measurements = pd.DataFrame([[0, 1.30, 1.40],
   ...                              [9, 1.60, 1.60],
   ...                              [18, 3.00, 1.65],
   ...                              [29, 3.90, 1.65],
   ...                              [39, 4.80, 2.00],
   ...                              [48, 5.20, 2.05],
   ...                              [51, 5.20, 2.15],
   ...                              [59, 4.90, 2.22],
   ...                              [69, 3.95, 2.42],
   ...                              [77, 2.95, 2.75],
   ...                              [82, 2.60, 3.42]],
   ...                             columns=['l', 'width', 'thickness'])

Step 2: Provide Material Data
:::::::::::::::::::::::::::::

In addition to bow measurements, an analysis method will also require some data about the bow’s material.
The Beckhoff method requires a wood species’ modulus of elasticity and its flexural strength/modulus of rupture, both in Pa.

We are going to use the values provided by Beckhoff:

* Modulus of elasticity: 8.875 GPa
* Modulus of rupture (compression): 61.29 MPa::

   >>> modulus_elasticity = 8.875E9
   >>> modulus_rupture = 61.29E6

Step 3: Feed the Analysis Function
::::::::::::::::::::::::::::::::::

The Beckhoff analysis function in pybow is called ``beckhoff1964()`` and is in the ``pybow.analysis`` subpackage.
It requires as inputs our measurements DataFrame, then the modulus of elasticity, then the modulus of rupture.
Its output will be a pandas.Series of results and a measurements DataFrame updated with all the properties it used for its computations::

   >>> results, new_measurements = pd.analysis.beckhoff1964(measurements,
   ...                                                      modulus_elasticity,
   ...                                                      modulus_rupture)

Step 4: Inspect and Interpret Results
:::::::::::::::::::::::::::::::::::::

The ``results`` variable holds the pandas.Series with all the properties the Beckhoff method estimated that bow to have.
Let’s Inspect it::

   >>> results
   drawweight     237.346616
   drawlength      73.386867
   braceheight     16.512045
   ntnlength      164.000000
   set              6.560000
   energy          71.745593
   dtype: float64


These numbers are all in SI units – length in cm, force in N – so they are unintuitive to us as archers or archery enthusiasts.

To convert draw weight and draw length into the more customary imperial units, we will have to divide them by their respective conversion factors.

* 4.448 N = 1 lbf.
* 2.54 cm = 1″::

   >>> results['drawweight']/4.448
   53.360300248
   >>> results['drawlength']/2.54
   28.8924674679

These values are looking both reasonable for a bow, and close to Beckhoff’s results.
