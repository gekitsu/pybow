Beckhoff 1964
:::::::::::::

Source
......

Beckhoff’s method for estimating a bow artefact’s usage properties was published in *Die Kunde*, the magazine of the archaeological society ‘Niedersächsischer Landesverein für Urgeschichte’ (en: Lower Saxony Society for Prehistory). [lsfug]_
It is described in the article ‘Der Eibenbogen von Vrees’ (en: the Vrees yew bow), pages 113–125 in volume 15 of the ‘new numbering’ (de: Neue Folge, abbreviated as N.F.), a restarting of their issue numbers in 1950. [kb1964]_

The article concerns itself with the Vrees artefact – a partial yew bow found in a bog in Germany.
In sequential chapters, it describes the circumstances of its find (pp. 113–114), its state of preservation (pp. 114–116), how Beckhoff calculates his estimations of its handling properties (pp. 117–121), the artefact’s dating by pollen (pp. 121–123), and closes with a debriefing and typological classification. (pp. 123–125)

Method
......

The general sequence of steps Beckhoff takes is:

#. Maximum permissible force at nock:
   How hard can you pull at the nock until the bow’s weakest spot reaches the material’s limit?
#. Amount of tip deflection:
   When this amount of force is applied to a beam of the bow’s stiffness, how far does it bend through?
#. Energy storage:
   How much energy does this bend store?
#. Draw length, draw weight, and other results:
   Given a brace height and a draw length derived from this bend, how much draw weight would the bow’s stiffness result in?

Beckhoff takes six measurements of the artefact’s one complete limb at distances :math:`l` ranging from 14.0 to 68.8 cm from the nock.
Based on scale drawings of the cross-sections at these points, Beckhoff determines second moment of area (:math:`I`) and distance from neutral plane to the flat belly (:math:`e`).

.. note::
   Second moment of area
      A property used to calculate the deflection of a beam.
      Think: stiffness.
      `Wikipedia <https://en.wikipedia.org/wiki/Second_moment_of_area>`__

   Neutral plane
      In a bending beam, a ‘middle’ plane through it that gets neither compressed nor stretched.
      `Wikipedia <https://en.wikipedia.org/wiki/Neutral_plane>`__

Beckhoff notes that each of the cross-sections deviates too much from a geometric shape to calculate :math:`I` and :math:`e` via formula; the more involved ‘tabular-graphical procedure’ (de: tabellarisch-zeichnerisches Verfahren) must be employed instead.
He does not describe this procedure.

Once there are :math:`I` and :math:`e` for each cross-section, the section modulus (:math:`W`) is calculated for each as :math:`W = \frac{I}{e}`.

.. note::

   Section modulus
      A property used to calculate the compression or tension stress for fibres in a bending beam.
      Think: how much bend until it ruptures or buckles?
      `Wikipedia <https://en.wikipedia.org/wiki/Section_modulus>`__

.. rubric:: Maximum permissible force at nock

With :math:`I`, :math:`e`, and :math:`W` for each cross-section, Beckhoff’s first step is to calculate the maximum permissible force that can pull on the nock for each cross-section to reach yew wood’s yielding strength under compression.
Most wood species can withstand much more tension stress than compression stress, therefore a bow is much more likely to fail by over-compressing the limbs’ belly sides than it is to fail by over-tensioning its back sides.
Beckhoff uses the ‘generally known’ (de: bekanntlich) Formula

.. math:: \frac{P \cdot l}{W} = \sigma_B

where:

:math:`P`
   The maximum permissible force to act on the nock.

:math:`l` (lowercase L)
   The distance of the cross-section from the nock.

:math:`P \cdot l`
   The bending momentum at :math:`l` when :math:`P` acts on the nock.

:math:`W`
   The cross-section’s section modulus.

:math:`\sigma_B`
   The bow material’s maximum compressive stress.

Calculating values of :math:`P` with yew wood’s :math:`\sigma_B` for each cross-section, Beckhoff can now identify the part of limb that would fail first, and what force at the nock would be necessary to induce failure.
Beckhoff chooses the lowest from his set of :math:`P`-values as the maximum permissible force to exert on the bow, and gently rounds it up – apparently to accomodate the bowyer’s dictum that a hint of compressive overstrain is seen as an indicator for a bow being built close enough to the limit to not be unnecessarily heavy.
Such an overstrain will not break the bow, but make it retain a small amount of permanent bend (called ‘set’ or ‘string-follow’) after unstringing it.

.. rubric:: Amount of tip deflection

The next step is to calculate the amount of deflection this maximum force will effect in the bow.
First, Beckhoff reformulates the problem from ‘one force acting upon each nock’ to ‘double that force acting upon the bow’s centre.’
He employs the ‘bending formula’ (de: Biegeformel):

.. math:: f = \frac{P \cdot l^3}{48 \cdot E \cdot I}

where:

:math:`f`
   The resultant maximum deflection.

:math:`P`
   The force acting upon the bow’s centre.
   (Twice the maximum permissible force at nock previously calculated)

:math:`l` (lowercase L)
   The bow’s effective, i.e. nock-to-nock length.

:math:`E`
   The bow material’s modulus of elasticity.
   Beckhoff reduces it to 75% to account for the Vrees bow’s material having flaws.

:math:`I` (uppercase i)
   The average second moment of area for the entire bow.
   It is arrived at by calculating :math:`\frac{l}{W}` from the constant :math:`\frac{\sigma_B}{P}`, yielding a :math:`W` for a :math:`l` at the bow’s centre.
   This :math:`W` can be used to derive a :math:`I` by a formula for an assumed geometric cross-section.
   The :math:`I` so derived would describe a beam of uniform bending stiffness, not a tapering beam like a bow, so Beckhoff multiples it with the ‘empirically obtained’ factor of 0.425.

.. warning::
   This average :math:`I` is a very strong simplification, further heightened by Beckhoff not explaining whether he sees this factor as empirically obtained for *this kind* of bow, or for bows in general.
   In any way, differences in how exactly a bow is tapered, and how that taper influences its bending behaviour, can not be taken into account with such a simplification – it treats all tapered beams as the same kind of variation of a constant width beam.

.. rubric:: Energy storage

Next, Beckhoff calculates the Energy stored by each limb with:

.. math:: A = \Big( f - \frac{s_p^2}{f} \Big)

where:

:math:`A`
   The stored energy (de: Arbeitsvermögen)

:math:`s_p`
   The brace height, i.e. the tip deflection at rest.
   Beckhoff just sets this value at 17 cm for the Vrees bow without explaining how he arrived at that value.
   While 17 cm is not exactly an unreasonable brace height, it is on the higher end of what bowyer literature suggests for primitive bows, see [tbb1strunk]_ p. 130, [tbb1baker]_ p.47f., [tbb4baker]_ p. 125, et al.

.. rubric:: Draw length, draw weight, and other results

Beckhoff approximates draw length as roughly twice the maximum permissible tip deflection, and can now calculatehis estimate for the bow’s draw weight with:

.. math:: A_{ges} = G \cdot P_z \cdot (draw\ length - brace\ height)

where:

:math:`A_{ges}`
   The energy stored by the whole (de: gesamt) bow, therefore twice the value for :math:`A` calculated above.

:math:`G`
   Beckhoff’s factor of ‘bow goodness’ (de: Bogengüte):
   A factor with a neutral value of 0.5, and describing a bow’s ‘character.’

.. note::
   Beckhoff’s ‘Bogengütefaktor’ :math:`G` is described in detail in [kb1963]_.
   It is defined as average draw force divided by a bow’s draw weight.
   The process is empirical:
   At regular draw distance intervals, draw force gets measured.
   These draw forces become averaged and then normalised by the bow’s draw weight.

   For Bows with a strictly linear increase in draw force, the average value will be exactly half the final draw weight, whereas convex force–draw curves will yield a :math:`G > 0.5`, concave force–draw curves will yield a :math:`G < 0.5`.

As final bow properties, Beckhoff calculates the set/string-follow the bow will take on as simply :math:`\frac{1}{25}` of the bow’s nock-to-nock length.

.. warning::
   Beckhoff’s estimation of how much string-follow a bow will acquire is not based on how highly a bow is stressed, how single overstressed points may propagate to different amounts of string-follow, depending on how far they are from the nock, etc.

To recapitulate the results Beckhoff’s estimating method yields:

* An assumed nock-to-nock length (extrapolated from one limb under assumption of symmetry)
* Maximum permissible draw length, theoretical
* Maximum permissible draw length, in practise (draw length plus string-follow)
* Draw weight at maximum permissible draw length
* Energy stored at maximum permissible draw

Implementation
..............

Implementing Beckhoff’s process is mostly very straightforward:
It is a clear sequence of steps, with formulas that are easy to translate into code.
As far as possible or reasonable, that is how I implemented Beckhoff’s method.

These are the issues where my implementation deviates from the text:

Beckhoff’s method for deriving :math:`I` and :math:`e` from visual representations of cross-sections can not be implemented since it is not described.
It also would not integrate well with a data model based on representing bow geometry via numerical measurements.

Therefore I chose to employ what Beckhoff cites as a viable alternative:
Calculate :math:`I` and :math:`e` with formulas for geometric shapes close enough to the actual cross-section shape.
The function calculating the values for the respective cross-section type is to be supplied as a keyword argument.

As a way in for more accurate values of :math:`I` and :math:`e`, I chose to only have them calculated formulaically if they are not supplied in the input.
Input-supplied values are interpreted as more accurate than formulaic approximations.

Other values not derived from calculations laid out in Beckhoff’s article are brace height and his ‘goodness factor’ :math:`G`.
For both, functions to guess approximations have been implemented, but value input via keyword argument is possible.

Since :math:`G` describes shapes of force–draw curves, and thereby how smooth or stacking a bow’s draw is, my guess is based on nock-to-nock length and draw length.

.. note::
   Stacking
      A phenomenon when drawing a bow becomes harder at an increasing rate compared to how far it is drawn back:
      Each equal increment in draw distance ‘costs’ more extra force than the previous one.
      It is linked to low energy storage for the draw weight and bow designs where limbs get drawn back far for their length.

Brace height is difficult to guess with authority.
While it has a minor influence on how highly limbs get stressed for an otherwise equal draw length, it is primarily a cultural preference.
Being forced to guess from material dimensions only, I chose to base it on draw length.
Draw length being more closely related to a bow’s ‘moving parts’ than nock-to-nock length (which may or may not include a stiff handle), that seemed the most sensible option.

Other deviations from Beckhoff’s method outline are:

* Beckhoff takes scattered measurements along the artefact and knows the length of the half-bow as a separate bit of information.
  Since it is both more simple from a data modelling point of view, I opted to derive limb/half-bow length from the measurements DataFrame instead.
  See :doc:`/discussions/design_limbconventions` for further information.
* Rounding up the maximum permissible force can be deactivated by keyword argument.
* Beckhoff’s reduction of :math:`E` is not implemented, as it is a measure to account for specifics of the Vrees bow’s material quality, not a general step in his calculation process.
* Instead of returning theoretical and practical draw length, as in the article, I am letting the function return (theoretical) draw length and set.
  If Beckhoff’s idea of practical draw length is desired (based on a non-strain-based amount of string-follow), adding them up manually is trivial enough.

....

.. [kb1964] Beckhoff, K 1964, ‘Der Eigenbogen von Vrees‘, Die Kunde N.F. 15, pp. 113–125.
.. [kb1963] Beckhoff, K 1963, ‘Die eisenzeitlichen Kriegsbogen von Nydam’, Offa 20, pp. 39–49.
.. [lsfug] Niedersächsischer Landesverein für Urgeschichte. URL: http://www.landesverein-urgeschichte.de/
.. [tbb1baker] Baker, T 1992: ‘Bow design and Performance’ in S Allely, T Baker, P Comstock, J Hamm, R Hardcastle, J Massey, J Strunk: *The Traditional Bowyer’s Bible. Volume One*, pp. 43–116. Bois D’Arc, Goldthwaite.
.. [tbb1strunk] Strunk, J 1992: ‘Yew Longbow’ in S Allely, T Baker, P Comstock, J Hamm, R Hardcastle, J Massey, J Strunk: *The Traditional Bowyer’s Bible. Volume One*, pp. 117–130. Bois D’Arc, Goldthwaite.
.. [tbb4baker] Baker, T 2008: ‘Bow design and Performance Revisited’ in S Allely, T Baker, P Comstock, S Gardner, J Hamm, M Lotz, T Mills, D Perry, M St. Louis, J Welch, M Westvang: *The Traditional Bowyer’s Bible. Volume Four*, pp. 113–158. Lyons, Guilford.
