Analysis Functions (``pybow.analysis``)
---------------------------------------

.. module:: pybow.analysis

Beckhoff 1964
:::::::::::::

.. function:: beckhoff1964(data, MoE, MoR[, name=None[, shape=halfround[, G=None[, braceheight=None[, strict=False]]]]])

   Calculate bow properties from artefact measurements after the method described in Beckhoff 1964. [kb1964]_
   See :doc:`/discussions/methods_beckhoff1964` for a description of the method.

   :param pandas.DataFrame data: A dataframe containing a sequence of measurements as described in :doc:`/discussions/design_limbconventions`.
   :param float MoE: The artefact material’s modulus of elasticity. (in Pa)
   :param float MoR: The artefact material’s modulus of rupture. (in Pa)
   :param str name: The artefact name.
                    If given, the result pandas.Series’ ``name`` attribute will be set to its value.
   :param function shape: A bundle function (see :doc:`02_beamtheory`) describing the cross-section shape to assume for the artefact.
   :param float G: A Beckhoff-G (see :doc:`/discussions/methods_beckhoff1964`) for the artefact.
                   If no value is given, a rough approximation will be estimated.
                   (default behaviour)
   :param float braceheight: A brace height for the artefact. (in cm)
                             If no value is given, a rough approximation will be estimated.
                             (default behaviour)
   :param bool strict: Indicated whether to calculate bending force strictly by maximum permissible stress or apply an approximation of Beckhoff’s rounding up beyond that level. (default behaviour)
   :rtype: tuple(pandas.Series, pandas.DataFrame):
   :returns: A tuple:

             * A pandas.Series containing the calculated bow properties.
             * A pandas.DataFrame based on ``data``, but with all additionally calculated properties.

Calculated Properties
.....................

:func:`beckhoff1964` calculates the following properties:

* Draw weight (Series key: ``drawweight``, in N)
* Draw length (Series key: ``drawlength``, in cm)
* Brace height (Series key: ``braceheight``, in cm)
* Effective length (Series key: ``ntnlength``, in cm)
* Set taken (Series key: ``stringfollow``, in cm)
* Energy stored (Series key: ``energy``, in J)

Data Requirements
.................

:func:`beckhoff1964` computes on the following properties:

* Second moment of area (``data['I']``)
* Neutral plane distance to belly (``data['e_belly']``)
* Section modulus towards belly (``data['W_belly']``)

Any of these columns not present in ``data`` will be calculated from ``data['width']`` and ``data['thickness']`` via ``shape``.

Junkmanns 2013
::::::::::::::

.. function:: junkmanns2013(data, correlationfactor[, spread=None[, name=None]])

   Calculate bow properties from artefact measurements after the method described in Junkmanns 2013. [jj2013]_
   See :doc:`/discussions/methods_junkmanns2013` for a description of the method.

   :param pandas.DataFrame data: A dataframe containing a sequence of measurements as described in :doc:`/discussions/design_limbconventions`.
   :param float correlationfactor: A Junkmanns correlation factor for the artefact material.
                                   (See :doc:`/discussions/methods_junkmanns2013`)
   :param float spread: Percentage of spread in correlation factors for artefacts of this material.
                        If supplied, low and high boundaries of draw weight get calculated.
                        They are ``NaN`` otherwise.
   :param str name: The artefact name.
                    If given, the result pandas.Series’ ``name`` attribute will be set to its value.
   :rtype: tuple(pandas.Series, pandas.DataFrame):
   :returns: A tuple:

             * A pandas.Series containing the calculated bow properties.
             * A pandas.DataFrame based on ``data``, but with all additionally calculated properties.

Calculated Properties
.....................

:func:`junkmanns2013` calculates the following properties:

* Draw weight (Series key: ``drawweight``, in N)
* Low end of draw weight spectrum (Series key: ``drawweight_low``, in N)
* High end of draw weight spectrum (Series key: ``drawweight_high``, in N)
* Draw length (Series key: ``drawlength``, in cm)
* Effective length (Series key: ``ntnlength``, in cm)

Data Requirements
.................

:func:`junkmanns2013` computes on the following properties:

* Junkmanns ‘Querschnittsstärke’ (``data['Qs']``)
* Junkmanns ‘Biegewiderstand’ (``data['Bw']``)

If these columns are not present in ``data``, they will get calculated from ``data['width']`` and ``data['thickness']`` after Junkmanns’ formula.

....

.. [kb1964] Beckhoff, K 1964, ‘Der Eigenbogen von Vrees‘, Die Kunde N.F. 15, pp. 113–125.
.. [jj2013] Junkmanns, J 2013, *Pfeil und Bogen*, Verlag Angelika Hörnig, Ludwigshafen.
