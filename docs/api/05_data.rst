Included Data (``pybow.data``)
------------------------------

.. module:: pybow.data

Data for Beckhoff 1964
::::::::::::::::::::::

.. data:: beckhoff1964

   A nested ``dict`` containing modulus of elasticity and modulus of rupture values used in Beckhoff’s 1964 paper. [kb1964]_
   
   It is structured on material names first, material properties second::

      {
          'yew': {
              'MoE': 11.77E9,
              'MoR': 61.29E6
          }
      }

.. data:: beckhoff1964_vrees1

   a :class:`pybow.Bow` with the measurements of the Vrees yew bow used in Beckhoff’s 1964 paper. [kb1964]_

Data for Junkmanns 2013
:::::::::::::::::::::::

.. data:: junkmanns2013

   A nested ``dict`` containing the correlation factors and their percentual spread as used in Junkmanns’ *Pfeil und Bogen*. [jj2013]_

   It is structured on material names first, correlation factor/spread second::

      {
          'yew': {
              'corrfac': 24.95328,
              'spread': 0.35
          },
          'elm': {
              'corrfac': 25.17568,
              'spread': 0.21
          }
      }

.. data:: junkmanns2013_rottenbottom

   A :class:`pybow.Bow` with the measurements of the Rotten Bottom bow used by Junkmanns to demonstrate his simplified method.

....

.. [kb1964] Beckhoff, K 1964, ‘Der Eigenbogen von Vrees‘, Die Kunde N.F. 15, pp. 113–125.
.. [jj2013] Junkmanns, J 2013, *Pfeil und Bogen*, Verlag Angelika Hörnig, Ludwigshafen.
