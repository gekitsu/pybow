Using Analysis Functions From the Command Line
----------------------------------------------

The core functions are also exposed via a command-line interface.
Its command name is ``pybow``, followed by the function name as a subcommand.
The function signature translates directly by these rules:

* mandatory function arguments become positional command-line arguments.

  * the argument taking a DataFrame will take  ``.csv`` file instead.

* optional function arguments/keyword arguments become optional arguments, named ``--argumentname``, with their default value preserved.
* optional function arguments accepting ``bool`` values:

  * arguments with a default value of ``False`` become standalone optional arguments named ``--argumentname``.
  * arguments with a default value of ``True`` become standalone optional arguments named ``--no-argumentname``

* the command-line argument ``-o``/``--output`` can be used to specify a file to write the output to. The default is ``stdout``.

.. rubric:: An example

A function with the signature::

   beckhoff1964(data, MoE, MoR, name=None, shape=halfround,
                G=None, braceheight=None, strict=False)

will be parsed into this terminal command::

   usage: pybow beckhoff1964 [-h] [-o OUTPUT] [--name NAME]
                             [--shape SHAPE] [--G G]
                             [--braceheight BRACEHEIGHT] [--strict]
                             data MoE MoR

   positional arguments:
     data
     MoE
     MoR

   optional arguments:
     -h, --help
     -o OUTPUT, --output OUTPUT
     --name NAME
     --shape SHAPE
     --G G
     --braceheight BRACEHEIGHT
     --strict

Note that the ``strict`` keyword argument defaults to ``False`` and must be set to ``True`` in function use.
In command-line use, not setting ``--strict`` equals ``strict=False``, while setting ``--strict`` equals ``strict=True``.
Were ``strict`` to default to ``True``, the command-line argument would be named ``--no-strict`` and including it in the command-line call equaled calling the function with ``strict=False``.
