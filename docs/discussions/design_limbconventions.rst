Limb Measurement Conventions
::::::::::::::::::::::::::::

Coming up with a standard for which measurements to require for a DataFrame describing a bow limb, and their format, was a central task in pybow’s planning phase.

These are the complete conventions for limb DataFrames in pybow:

* A DataFrame describing a bow limb contains multiple measurements taken along the bow.
  Each measurement is a row, and each measured property is a column.
* As a required minimum, DataFrames must contain columns ``l``, ``width``, and ``thickness``.
* ``width`` and ``thickness`` contain limb width and thickness measurements for each point.
  Their unit is cm.
* ``l`` contains the measured profile’s distance from the bow nock, in cm.
  The nock has a value of ``l == 0``.
  Protrusions beyond the nock have negative values.
  Measured profiles should cover the bow from nock to midpoint.
* Additional columns are allowed.

Here is the how and why:

The literature for pybow’s initially implemented methods offer differing conventions on this matter.
Both [kb1964]_ and [jj2013]_ agree in their approach of describing a bow by taking measurements at several points along the artefact’s length.
The measurements taken are the respective basic properties for their approaches, and a measurement of position for each point.

Junkmanns’ basic measurements are limb width and thickness, Beckhoff’s are second moment of area and neutral plane distance to belly for each cross-section.
While Beckhoff measures each point’s position by its distance from the artefact’s nock, Junkmanns measures by distance from the artefact’s centre.

Since Beckhoff’s required measurements can be approximated from limb width and thickness, they make reasonable minimum requirements for limb DataFrames.
They are also significantly easier to obtain than second moment of area, etc.
Junkmanns’ measurement tables show longitudinal position as distance from bow centre, but his calculations actually use distance from nock – he converts measurements silently and implicitly.

As distance from nock makes more sense in most cases, and longitudinal measurements are fairly easy to convert anyway, distance from nock sounds like the best choice for encoding position.

If the nock marks the spot where ``l == 0``, protrusions of the limb above the nock should be encoded as having a negative ``l`` value.
That way, the working parts of the limb can be readily identified as having values ``l >= 0``, and ``l``-measurements can be converted completely to a Junkmanns notation.

Junkmanns’s measurement tables, as a rule, cover the entire artefact, from midpoint to end.
That means bow length can be inferred from the tables alone.
Conversely, Beckhoff takes a few measurements and a separate measure of distance from nock to midpoint.
I prefer Junkmanns’ approach because it is structurally simpler.
That it enforces a complete coverage of measurements is an added bonus – Beckhoff’s approach makes it much easier to miss whether a bow part not covered by measurements does or does not bend.

