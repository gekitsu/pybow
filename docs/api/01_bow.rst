Bow and Limb Objects
--------------------

.. module:: pybow

Bow Objects
:::::::::::

.. class:: Bow(name, source[, tags=()[, limbs=()[, \*attrs]]])

   Describes a bow by collecting measurements and metadata.
   Its data attributes :attr:`name` and :attr:`source` are mandatory for initialisation, while :attr:`material` et al. are optional.

   :param str name: The bow’s :attr:`name`.
   :param str source: The bow’s :attr:`source`.
   :type tags: list[str]
   :param tags: A list of tags to initialise the bow’s :attr:`tags` with.
   :type limbs: list[pandas.DataFrame] or list[list]
   :param limbs: A list of arguments for the :meth:`add_limbs` method, to add limbs to the bow on initialisation.
               Either a list of ``pandas.DataFrame``\s, or a list of lists with complete inputs for .meth:`add_limbs`.

   .. attribute:: abslength

      :returns: The overall length of the bow, as derived from its :attr:`Limb.data`.
                This includes incomplete limbs and protrusions beyond the nock.
      :rtype: float

   .. attribute:: date

      A date identifier for sorting or grouping.

   .. attribute:: description

      A free text metadata field for an extended description of the bow.
      
   .. attribute:: material

      The name of the bow’s material.
      What language to use for identifying wood species is unspecified.

   .. attribute:: name

      A name describing the bow.

   .. attribute:: ntnlength

      :returns: The effective length of the bow, from nock to nock, as derived from its :attr:`Limb.data`.
                ``NaN`` if the bow has fewer than 2 complete limbs.
      :rtype: float or NaN

   .. attribute:: source
   
      A statement identifying the source of the bow data.
      Depending on the bow described, this can be the name of its maker/designer, literature, or similar.

   .. attribute:: tags

      A set of strings acting as categorical descriptors.
      For more information on the tag system, see :doc:`/howto/tags`.


   .. method:: add_limb(df, complete=True, tags=())
   
      Add a :py:class:`Limb` to the bow.

      :param pandas.DataFrame df: The limb’s :attr:`Limb.data` DataFrame with measurements.
      :param bool complete: The limb’s :attr:`Limb.complete`.
      :param list[str] tags: Tags to attach to the limb’s :attr:`Limb.tags`.
      :raises TypeError: When trying to exceed the maximum number of two limbs per bow.
      :raises ValueError: Via :class:`Limb`, when the limb DataFrame does not conform to minimum specifications.
                          (See :attr:`Limb.data`)

   .. method:: del_limb(ix)
      
      Delete a limb from the bow.

      :param int ix: The index position of the limb to be deleted.
      :raises IndexError: For values of ``ix`` with no correlating limb in the bow.

   .. method:: write(fname)
      
      Write the bow object to disk.
      Up to three files get written:

      * an ``fname.yaml`` containing the metadata and a manifest
      * up to two ``fname_n.csv`` files containing the limb measurement DataFrames.

      :param str fname: A filename of file basename.
                        If a full filename is given, its extension will be ignored.

Limb Objects
::::::::::::

.. class:: Limb()

   Describes a bow limb, or more precisely: a half-bow.
   These objects generally don’t get created directly, but rather through :meth:`Bow.add_limb`.

   :raises ValueError: When trying to initialise a limb with a DataFrame that does not conform to minimum specifications.
                       (See :attr:`data`)

   .. attribute:: abslength

      :returns: The limb’s absolute length, including protrusions beyond the nock.
      :rtype: float or int

   .. attribute:: data

      The DataFrame containing limb measurements.
      It must contain, at a minimum, contain columns named ``l``, ``width``, and ``thickness``.
      See :doc:`/discussions/design_limbconventions` for further information.

   .. attribute:: complete

      A ``bool`` indicating whether the :attr:`data` describes a whole limb or an incomplete limb.

   .. attribute:: length

      :returns: The limb’s effective length, i.e. from a point with :math:`l = 0` to the maximum value of :math:`l`.

   .. attribute:: name

      :returns: A limb name comprised of :attr:`Bow.name` and the limb’s index number.
      :rtype: str

   .. attribute:: material

      :returns: The parent :attr:`Bow.material`.
      :rtype: str

   .. attribute:: tags

      A set of strings acting as categorical descriptors.
      For more information on the tag system, see :doc:`/howto/tags`.

Indexing and Iterating
::::::::::::::::::::::

Bow objects behave to their limbs like lists behave to their items:

Indexing by number::

   >>> my_bow[0]
   [pd.DataFrame(...), True, set('some_tag', 'another_tag')]

Iterating::

   >>> for l in my_bow:
   ...     print(l.name)
   ...
   'my_bow_name_0'
   'my_bow_name_1'

And also::

   >>> len(my_bow)
   2

Read/Write
::::::::::

.. function:: read(fname)

   Reads a bow object from the files :meth:`Bow.write` writes.

   :param str fname: The file name of a bow’s ``.yaml`` file.
   :returns: A bow object.
   :rtype: :class:`Bow`

See also: :meth:`Bow.write`.
