#!/usr/bin/env python3
from context import pybow as pb
import pytest

import pandas as pd

# test each function that does more than simple math on its inputs
# so far, there are none such function for pb.analysis.beckhoff1964()


# approximation test for beckhoff1964 with vrees artefact data
@pytest.fixture
def beckhoff_vrees():
    return pd.DataFrame([[14.0, 0.36, 0.6],
                         [19.3, 0.59, 0.65],
                         [39.8, 1.84, 0.81],
                         [48.0, 2.41, 0.86],
                         [56.3, 2.64, 0.91],
                         [68.6, 3.0, 1.0],
                         [85.0, 5.71, 1.45]],
                        columns=['l', 'I', 'e_belly'])


def test_beckhoff1964(beckhoff_vrees):
    bh_moe = 0.75 * 11.77E9
    bh_mor = 6.129E7
    beckhoff_results, _ = pb.analysis.beckhoff1964(beckhoff_vrees,
                                                   bh_moe, bh_mor,
                                                   name='Vrees')
    assert beckhoff_results['drawweight'] * 0.2248 == pytest.approx(60, abs=3)
    assert beckhoff_results.name == 'Vrees'
