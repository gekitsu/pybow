#!/usr/bin/env python3
from context import pybow as pb
import pytest

import numpy as np

# CONSTANTS & FIXTURES

WIDTH = 4
THICKNESS = 2
RATIO = 0.5

RECT = {'I': 32/12,
        'W': 16/6,
        'e_back': 1,
        'e_belly': 1}
ROUND = {'I': np.pi/2,
         'W': np.pi/2,
         'e_back': 1,
         'e_belly': 1}
HALFROUND = {'I': 0.10976*(WIDTH/2)*THICKNESS**3,
             'W': 0.19065*(WIDTH/2)*THICKNESS**2,
             'e_back': 0.5756*THICKNESS,
             'e_belly': THICKNESS - 0.5756*THICKNESS}
TRAP50 = {'I': 52/6 * 8/36,
          'W': 52/10 * 4/12,
          'e_back': 10/6 * 2/3,
          'e_belly': THICKNESS - (10/6 * 2/3)}
TRAP66 = {'I': (((2/3)*4)**2 + 4*(2/3)*4*4 + 16)/((2/3)*4 + 4) * 8/36,
          'W': (((2/3)*4)**2 + 4*(2/3)*4*4 + 16)/((2/3)*4 + 8) * 4/12,
          'e_back': ((2/3)*4 + 8)/((2/3)*4 + 4) * 2/3,
          'e_belly': THICKNESS - ((2/3)*4 + 8)/((2/3)*4 + 4) * 2/3}
TRAP75 = {'I': 73/7 * 8/36,
          'W': 73/11 * 4/12,
          'e_back': 11/7 * 2/3,
          'e_belly': THICKNESS - (11/7 * 2/3)}


# TESTS
def test_rect_funcs():
    assert pb.rect_I(WIDTH, THICKNESS) == RECT['I']
    assert pb.rect_W(WIDTH, THICKNESS) == RECT['W']
    assert pb.rect_e_back(WIDTH, THICKNESS) == RECT['e_back']
    assert pb.rect_e_belly(WIDTH, THICKNESS) == RECT['e_belly']
    assert pb.rect_e(WIDTH, THICKNESS) == RECT['e_back']


def test_round_funcs():
    assert pb.round_I(WIDTH, THICKNESS) == ROUND['I']
    assert pb.round_W(WIDTH, THICKNESS) == ROUND['W']
    assert pb.round_e_back(WIDTH, THICKNESS) == ROUND['e_back']
    assert pb.round_e_belly(WIDTH, THICKNESS) == ROUND['e_belly']
    assert pb.round_e(WIDTH, THICKNESS) == ROUND['e_back']


def test_halfround_funcs():
    assert pb.halfround_I(WIDTH, THICKNESS) == \
        pytest.approx(HALFROUND['I'], rel=1e-3)
    assert pb.halfround_W(WIDTH, THICKNESS) == \
        pytest.approx(HALFROUND['W'], rel=1e-3)
    assert pb.halfround_e_back(WIDTH, THICKNESS) == \
        pytest.approx(HALFROUND['e_back'], rel=1e-3)
    assert pb.halfround_e_belly(WIDTH, THICKNESS) == \
        pytest.approx(HALFROUND['e_belly'], rel=1e-3)
    assert pb.halfround_e(WIDTH, THICKNESS) == \
        pytest.approx(HALFROUND['e_back'], rel=1e-3)


def test_english_funcs():
    assert pb.english_I(WIDTH, THICKNESS) == \
        pytest.approx(HALFROUND['I'], rel=1e-3)
    assert pb.english_W(WIDTH, THICKNESS) == \
        pytest.approx(HALFROUND['W'], rel=1e-3)
    assert pb.english_e_back(WIDTH, THICKNESS) == \
        pytest.approx(HALFROUND['e_belly'], rel=1e-3)
    assert pb.english_e_belly(WIDTH, THICKNESS) == \
        pytest.approx(HALFROUND['e_back'], rel=1e-3)
    assert pb.english_e(WIDTH, THICKNESS) == \
        pytest.approx(HALFROUND['e_back'], rel=1e-3)


def test_trap_funcs():
    assert pb.trap_I(WIDTH, THICKNESS, RATIO) == TRAP50['I']
    assert pb.trap_W(WIDTH, THICKNESS, RATIO) == TRAP50['W']
    assert pb.trap_e_back(WIDTH, THICKNESS, RATIO) == TRAP50['e_back']
    assert pb.trap_e_belly(WIDTH, THICKNESS, RATIO) == TRAP50['e_belly']
    assert pb.trap_e(WIDTH, THICKNESS, RATIO) == TRAP50['e_back']


def test_rect_bundlefunc():
    assert pb.rect(WIDTH, THICKNESS) == (RECT['I'], RECT['W'], RECT['e_back'])
    assert pb.rect(WIDTH, THICKNESS, ['e_back', 'e_belly']) == \
        (RECT['e_back'], RECT['e_belly'])


def test_round_bundlefunc():
    assert pb.round(WIDTH, THICKNESS) == \
        (ROUND['I'], ROUND['W'], ROUND['e_back'])
    assert pb.round(WIDTH, THICKNESS, ['e_back', 'e_belly']) == \
        (ROUND['e_back'], ROUND['e_belly'])


def test_halfround_bundlefunc():
    assert pb.halfround(WIDTH, THICKNESS) == \
        pytest.approx((HALFROUND['I'], HALFROUND['W'], HALFROUND['e_back']),
                      rel=1e-3)
    assert pb.halfround(WIDTH, THICKNESS, ['e_back', 'e_belly']) == \
        pytest.approx((HALFROUND['e_back'], HALFROUND['e_belly']),
                      rel=1e-3)


def test_english_bundlefunc():
    assert pb.english(WIDTH, THICKNESS) == \
        pytest.approx((HALFROUND['I'], HALFROUND['W'], HALFROUND['e_back']),
                      rel=1e-3)
    assert pb.english(WIDTH, THICKNESS, ['e_back', 'e_belly']) == \
        pytest.approx((HALFROUND['e_belly'], HALFROUND['e_back']),
                      rel=1e-3)


def test_trap50_bundlefunc():
    assert pb.trap50(WIDTH, THICKNESS) == \
        (TRAP50['I'], TRAP50['W'], TRAP50['e_back'])
    assert pb.trap50(WIDTH, THICKNESS, ['e_back', 'e_belly']) == \
        (TRAP50['e_back'], TRAP50['e_belly'])


def test_trap66_bundlefunc():
    assert pb.trap66(WIDTH, THICKNESS) == \
        (TRAP66['I'], TRAP66['W'], TRAP66['e_back'])
    assert pb.trap66(WIDTH, THICKNESS, ['e_back', 'e_belly']) == \
        (TRAP66['e_back'], TRAP66['e_belly'])


def test_trap75_bundlefunc():
    assert pb.trap75(WIDTH, THICKNESS) == \
        (TRAP75['I'], TRAP75['W'], TRAP75['e_back'])
    assert pb.trap75(WIDTH, THICKNESS, ['e_back', 'e_belly']) == \
        (TRAP75['e_back'], TRAP75['e_belly'])
