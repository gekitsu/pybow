.. pybow documentation master file, created by
   sphinx-quickstart on Tue Jun 19 06:20:46 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pybow: Archery Computation in Python
====================================

**pybow** is a Python package collecting methods for performing helpful calculations on archery bows.

Right now, these calculations are of an analytical nature only – deriving knowledge about a bow from its measurements.
Future expansions are planned.

Documentation
-------------

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents

   tutorial
   howto
   apidoc
   discussions

Indices and Tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
