Junkmanns 2013
::::::::::::::

Source
......

Junkmanns’ method was published in his doctoral dissertation *Pfeil und Bogen* (en: ‘bow and arrow’), an attempt to bring information on all relevant archery artefacts from the paleolithic to the middle ages up to date, as a reaction to existing literature being both out of date, and often factually incorrect. ([jj2013]_ p. xii)
He therefore set out to accurately describe the 2013 state of relevant archery-related finds, devoting the bulk of his book (~300 pages) to these descriptions, and grounding their interpretation in his extensive practical understanding of bowyery and archery.

The relevant section describing Junkmanns’ ‘simplified method for bow calculation‘ (de: eine vereinfachte Methode zur Bogenberechnung) is situated in the context of a larger chapter on how to gather knowledge about the power of (pre)historic bows. (pp. 63–78)
This chapter comprises of two main approaches:
One is practical – build reconstructions and measure/test them – the other is theoretical – take measurements of the artefact and calculate the desired values.
In the subsection on theoretical approaches, Junkmanns describes and/or mentions various methods for calculating bow properties, :doc:`methods_beckhoff1964` among them. (pp. 72–74)

Coming to the conclusion that every approach, from reproduction to even Bob Kooi’s advanced theoretical models suffers from similar problems, namely that their results are only as representative of (pre)historic bows as the material used, or the material properties employed, are representative of old material, and that each rebuilding and modeling makes assumptions about usage and technique, he concedes that there is no way to derive completely accurate performance assessments from the artefacts.
Nevertheless, Junkmanns wants to at least estimate draw weight in a systematic manner that is practicable enough to employ for an artefact corpus of *Pfeil und Bogen*\ ’s scale.

Method
......

Junkmanns’ ‘simplified method’ is aptly simple, and visibly inspired by Beckhoff’s method:

#. Abstract profile strength:
   How stiff is each cross-section?
#. Abstract bending resistance:
   How does this stiffness behave when placed at its position in the limb?
#. Abstract bow strength:
   Aggregating these bending resistance values to one bow strength value.
#. The correlation factor:
   How does an abstract value translate into an amount of force?

Junkmanns begins with a table of measurements quite similar to Beckhoff’s, featuring longitudinal position of a cross-section, its width and thickness.
As opposed to Beckhoff, Junkmanns measures distance from bow centre, not distance from nock.

.. rubric:: Abstract profile strength

Junkmanns calculates a bending strength factor :math:`Qs` (from de: Querschnittsstärke, cross-section strength) for each cross-section.
It is based on the section modulus :math:`W`, but simplified to :math:`width \cdot thickness^2` (both in cm) under the assumption that cross-section shape is a negligible influence next to width and especially thickness.

.. warning:: 
   Junkmanns mistakenly assumes the section modulus :math:`W` to describe a beam’s stiffness, instead of the second moment of area :math:`I`.
   Since :math:`I` weighs thickness to the power of 3 instead of :math:`W`’s power of 2, Junkmanns’ approach will underestimate the role of thickness.

   He even explicitly states that a bow twice as thick, all other measurements equal, would be 4 times as stiff (:math:`2^2`) instead of 8 times (:math:`2^3`). (p. 75)
   The correct proportion is quoted in standard bowyery texts, such as [tbb1hamm]_ p. 259.

   The source of this misunderstanding may lie in a misread of Beckhoff’s method:
   Unless the reader pays attention to what each formula does, it can appear as if Beckhoff uses :math:`I` and :math:`e` only to calculate :math:`W`, without noticing that :math:`W` gets used only to find overstrain boundaries, and calculation of energy storage and draw weight resumes based on :math:`I`.

.. rubric:: Abstract bending resistance

To determine how each cross-section’s strength influences bending behaviour, Junkmanns calculates a value :math:`Bw` (from de: Biegewiderstand, bending resistance) as:

.. math:: Bw = \frac{d}{Qs}

where:

:math:`d`
   Distance from nock. (in cm)

.. note::
   :math:`Bw` is scaled inversely:
   Smaller values indicate high resistance to bending (short lever and/or high profile strength), larger values indicate low resistance to bending (long lever and/or low profile strength).

Junkmanns’ :math:`Bw` seems very obviously inspired by Beckhoff’s :math:`\frac{l}{W}`.
When interpreting artefacts where distribution of bend along the bow is of interest, he uses :math:`Bw` as an indicator.

.. rubric:: Abstract bow strength

To find a value signifying a bow’s overall strength, Junkmanns uses the formula:

.. math:: Bs = \frac{1}{mean(Bw) \cdot ntnlength^2} \cdot 100

where:

:math:`Bs`
   Overall bow strength.
   (de: Bogenstärke)

:math:`mean(Bw)`
   The arithmetic mean of all :math:`Bw` values.
   Due to its inverse scaling, it must be inverted.

:math:`ntnlength`
   The nock-to-nock distance of a bow. (in m)

.. rubric:: The correlation factor

Since so far, all of Junkmanns’ values are unitless and abstract, a factor expressing the correlation between abstract values and real-world draw weights is needed.
Junkmanns derives these empirically by calculating :math:`Bs` for bow reconstructions and measuring their real draw weight (in lbf.).
He generates a mean value both for yew bows (based on 20 measured bows) and elm bows (based on 6 measured bows), and notes the ‘variability’ in the calculated correlation factors as a percentage of deviation.
These values are fairly large: +/- 35% for yew, +/- 21% for elm.

Finally, Junkmanns takes into account that smaller bows will have a shorter draw length, and therefore their draw weight will be a fraction of the calculated value.
This is done via a simple table lookup, yielding a draw length and draw weight reduction percentage for a given nock-to-nock length.

.. warning::
   Junkmanns’ correlation value means are not taking into account the actual spread of his measured values!
   The mean values presented are the arithmetic mean of the value spread’s minimum and maximum values only.

   Junkmanns’ ‘variability’ percentage then is the difference of his min/max mean and these min/max values, not a variance or standard deviation.

Implementation
..............

For the most part, Junkmanns’ ‘simplified method’ can be implemented straightforward – its calculations are trivial and the sequence of steps is not complex either.

These are the deviations from the text:

Since Junkmanns’ measurement tables record distance from bow centre, but he implicitly converts these values to distance from nock for calculating :math:`Bw`, input format has been changed to expect position in distance from nock, not distance from centre.
See also: :doc:`/discussions/design_limbconventions`

Junkmanns does not calculate :math:`Qs` and :math:`Bw` for the nock points, and therefore does not include them in the :math:`mean(Bw)`.
This would be simple to implement, but he occasionally inserts interesting additional measuring points, like the protrusions at the Vrees bow’s nock, without calculating :math:`Qs` and :math:`Bw` for them either.
Therefore, I opted to exclude measuring points with a distance from nock smaller or equal to 2 cm instead of just the nock point.
This should be close enough a margin to not interfere with Junkmanns’ regular practise of taking measurements every 5–10 cm.

Junkmanns can manually account for having one or two complete limbs to include in his :math:`mean(Bw)`, and whether to derive nock-to-nock length from artefact measurements versus inferring it from doubling distance from bow centre to a single limb’s nock.
In code, this would complicate a structurally simple function, so I left the possibility of submitting two limb DataFrames unimplemented for the time being.

Despite a Junkmanns correlation factor resulting in a draw weight in lbf. (pounds-force), for reasons of package-wide standardisation to SI units, result draw weight is still given in N.

....

.. [jj2013] Junkmanns, J 2013, *Pfeil und Bogen*, Verlag Angelika Hörnig, Ludwigshafen.
.. [tbb1hamm] Hamm, J 1992: ‘Tillering’ in S Allely, T Baker, P Comstock, J Hamm, R Hardcastle, J Massey, J Strunk: *The Traditional Bowyer’s Bible. Volume One*, pp. 257–286. Bois D’Arc, Goldthwaite.
